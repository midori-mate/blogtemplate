
"""BlogTemplate

ブログテンプレートを作るにあたり、ヘッダとかフッタとか共通パーツの分割がしたかったので bottle を使う。
http://localhost:8000/index.html でアクセスしてね。
"""

from bottle import route, run, template, request
from bottle import error, HTTPResponse, static_file
import requests
import time
import datetime
import json
import random


# 静的ファイル。
@route('/static/<file_path:path>')
def static(file_path):
    return static_file(file_path, root='./static')


# トップページ。
@route('/')
@route('/index.html')
def index():

    # get_url_info = requests.get('https://gitlab.com/users/midori-mate/calendar.json')
    # json_str = '{"2018-09-17":5,"2018-09-29":10,"2018-10-24":4,}'
    # dic = {}
    # for key, value in json.loads(json_str).items():
    #     dic[datestr_to_timestamp(key)] = value
    sample_activity_dic = create_sample_activity_dic()

    return template('index', heatmap_json=json.dumps(sample_activity_dic))


# yyyy-mm-dd をタイムスタンプ(int)に変換。
def datestr_to_timestamp(datestr):
    return int(time.mktime(datetime.datetime.strptime(datestr, '%Y-%m-%d').timetuple()))


# [今日の4ヶ月前] ～ [今日] の {日付タイムスタンプ:ランダム数値} ディクショナリ作成。
def create_sample_activity_dic():

    # 4ヶ月前(16週間前とする)を取得。
    four_months_ago = datetime.datetime.today() - datetime.timedelta(days=112)

    # から今日をリストで取得。
    lis = []
    for i in range(112):
        d = four_months_ago + datetime.timedelta(days=i)
        lis.append(d.strftime('%Y-%m-%d'))

    # 日付タイムスタンプ:ランダム数値 ディクショナリを作成。
    dic = {}
    for l in lis:
        dic[datestr_to_timestamp(l)] = random.randint(0, 40)

    return dic


# 記事ページ。
@route('/article.html')
def article():
    return template('article')


run(host='0.0.0.0', port=8000, debug=True, reloader=True)
