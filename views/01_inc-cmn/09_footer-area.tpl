
<!-- ***** Footer Area Start ***** -->
<footer class="footer-area mrrhp-bgcolor black">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-4">
        <div class="footer-single-widget">
          <a href="#"><img src="/static/img/logo/mrrhp-logo-black.png" alt=""></a>
          <div class="copywrite-text mt-30">
            <p>
              Site: Mirori-HP
            </p>
            <p>
              Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved
            </p>
            <p>
              This site is made by Midoriiro
            </p>
            <p>
              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
              This template was made by <a href="https://colorlib.com">Colorlib</a>
            </p>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-4">
        <div class="footer-single-widget">
          <ul class="footer-menu d-flex justify-content-between">
            <li><a href="#"><i class="fa fa-home"></i> HOME</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> タグ1</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> タグ2</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> タグ3</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> タグ4</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> タグ5</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> タグ6</a></li>
          </ul>
        </div>
      </div>
      <div class="col-12 col-md-4">

      </div>
    </div>
  </div>
</footer>
<!-- ***** Footer Area End ***** -->

<!-- jQuery (Necessary for All JavaScript Plugins) -->
<script src="/static/js/jquery/jquery-2.2.4.min.js"></script>
<!-- Popper js -->
<script src="/static/js/popper.min.js"></script>
<!-- Bootstrap js -->
<script src="/static/js/bootstrap.min.js"></script>
<!-- Plugins js -->
<script src="/static/js/plugins.js"></script>
<!-- Dropotron js -->
<script src="/static/js/jquery.dropotron.min.js"></script>
<!-- Active js -->
<script src="/static/js/active.js"></script>

<a id="nextPage" href="#" title="次の記事へ">
  <i class="fa fa-arrow-right"></i>
</a>
<a id="prevPage" href="#" title="前の記事へ">
  <i class="fa fa-arrow-left"></i>
</a>

<!-- Prettify -->
<script src="/static/js/prettify.js"></script>
<script src="/static/js/lang-css.js"></script>
<script>prettyPrint();</script>
<!-- cal-heatmap js -->
<script src="/static/libs/d3.min.js"></script>
<script src="/static/libs/cal-heatmap/cal-heatmap.min.js"></script>
