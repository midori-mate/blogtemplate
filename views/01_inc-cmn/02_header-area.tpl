
<!-- Preloader Start -->
<div id="preloader">
<div class="preload-content">
  <div id="world-load"></div>
</div>
</div>
<!-- Preloader End -->

<!-- ***** Header Area Start ***** -->
<header class="header-area">
<div class="container">
  <div class="row">
    <div class="col-12">
      <nav id="nav" class="navbar navbar-expand-lg">
        <!-- Logo -->
        <a class="navbar-brand" href="#">
          <img src="/static/img/logo/mrrhp-logo-white.png" alt="Logo" style="width:auto; height:25px;">
        </a>
        <!-- Navbar Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#worldNav" aria-controls="worldNav" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <!-- Navbar -->
        <div class="collapse navbar-collapse" id="worldNav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">
                <i class="fa fa-home"></i> HOME
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-tags"></i>  TAGS
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="">タグ1(7)</a>
                <a class="dropdown-item" href="">タグ2(89)</a>
                <a class="dropdown-item" href="">タグ3(34)</a>
                <a class="dropdown-item" href="">タグ4(15)</a>
                <a class="dropdown-item" href="">タグ5(100)</a>
                <a class="dropdown-item" href="">タグ6(23)</a>
              </div>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-calendar"></i> CALENDAR
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="">2018(9)</a>
                <a class="dropdown-item" href="">2017(45)</a>
                <a class="dropdown-item" href="">2016(65)</a>
                <a class="dropdown-item" href="">2015(32)</a>
                <a class="dropdown-item" href="">2014(10)</a>
                <a class="dropdown-item" href="">2013(5)</a>
              </div>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                TPL
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="/index.html">INDEX</a>
                <a class="dropdown-item" href="/article.html">ARTICLE</a>
              </div>
            </li>
          </ul>
          <!-- Search Form  -->
          <div id="search-wrapper">
            <form action="#">
              <input type="text" id="search" placeholder="Search something...">
              <div id="close-icon"></div>
              <input class="d-none" type="submit" value="">
            </form>
          </div>
        </div>
      </nav>
    </div>
  </div>
</div>
</header>
<!-- ***** Header Area End ***** -->

