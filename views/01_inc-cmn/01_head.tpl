
<meta charset="UTF-8">
<meta name="description" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

<!-- Title  -->
<title>みろりHP</title>

<!-- Favicon  -->
<link rel="icon" href="/static/img/logo/favicon.ico">

<!-- Style CSS -->
<link rel="stylesheet" href="/static/css/style.css">
<!-- Custom -->
<link rel="stylesheet" href="/static/css/style-custom.css">
<!-- Github Markdown -->
<link rel="stylesheet" href="/static/css/github-markdown.css">
