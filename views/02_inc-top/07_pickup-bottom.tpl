
<div class="row justify-content-center">

  <!-- ========== Single Blog Post ========== -->
  <div class="col-12 col-md-6 col-lg-4">
    <div class="single-blog-post post-style-3 mt-50 wow fadeInUpBig" data-wow-delay="0.1s">
      <!-- Post Thumbnail -->
      <div class="post-thumbnail">
        <img src="/static/img/blog-img/b15.jpg" alt="">
        <!-- Post Content -->
        <div class="post-content d-flex align-items-center justify-content-between">
          <!-- Catagory -->
          <div class="post-tag"></div>
          <!-- Headline -->
          <a href="#" class="headline">
            <h5>[ピックアップ記事] 記事タイトル</h5>
          </a>
          <!-- Post Meta -->
          <div class="post-meta">
            <p><a href="#" class="post-author">書いた人</a> on <a href="#" class="post-date">日付</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- ========== Single Blog Post ========== -->
  <div class="col-12 col-md-6 col-lg-4">
    <div class="single-blog-post post-style-3 mt-50 wow fadeInUpBig" data-wow-delay="0.1s">
      <!-- Post Thumbnail -->
      <div class="post-thumbnail">
        <img src="/static/img/blog-img/b16.jpg" alt="">
        <!-- Post Content -->
        <div class="post-content d-flex align-items-center justify-content-between">
          <!-- Catagory -->
          <div class="post-tag"></div>
          <!-- Headline -->
          <a href="#" class="headline">
            <h5>[ピックアップ記事] 記事タイトル記事タイトル</h5>
          </a>
          <!-- Post Meta -->
          <div class="post-meta">
            <p><a href="#" class="post-author">書いた人</a> on <a href="#" class="post-date">日付</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- ========== Single Blog Post ========== -->
  <div class="col-12 col-md-6 col-lg-4">
    <div class="single-blog-post post-style-3 mt-50 wow fadeInUpBig" data-wow-delay="0.1s">
      <!-- Post Thumbnail -->
      <div class="post-thumbnail">
        <img src="/static/img/blog-img/b17.jpg" alt="">
        <!-- Post Content -->
        <div class="post-content d-flex align-items-center justify-content-between">
          <!-- Catagory -->
          <div class="post-tag"></div>
          <!-- Headline -->
          <a href="#" class="headline">
            <h5>[ピックアップ記事] 記事タイトル記事タイトル記事タイトル</h5>
          </a>
          <!-- Post Meta -->
          <div class="post-meta">
            <p><a href="#" class="post-author">書いた人</a> on <a href="#" class="post-date">日付</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- ========== Single Blog Post ========== -->
  <div class="col-12 col-md-6 col-lg-4">
    <div class="single-blog-post post-style-3 mt-50 wow fadeInUpBig" data-wow-delay="0.1s">
      <!-- Post Thumbnail -->
      <div class="post-thumbnail">
        <img src="/static/img/blog-img/b18.jpg" alt="">
        <!-- Post Content -->
        <div class="post-content d-flex align-items-center justify-content-between">
          <!-- Catagory -->
          <div class="post-tag"></div>
          <!-- Headline -->
          <a href="#" class="headline">
            <h5>[ピックアップ記事] 記事タイトル記事タイトル記事タイトル記事タイトル</h5>
          </a>
          <!-- Post Meta -->
          <div class="post-meta">
            <p><a href="#" class="post-author">書いた人</a> on <a href="#" class="post-date">日付</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
