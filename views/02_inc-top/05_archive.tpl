
<div class="sidebar-widget-area">
  <h5 class="title">Archives</h5>
  <div class="widget-content">

    <!-- Single Blog Post -->
    <div class="single-blog-post post-style-2 d-flex align-items-center widget-post">
      <!-- Post Thumbnail -->
      <div class="post-thumbnail">
        <a href="">
          <img src="/static/img/blog-img/b10.jpg" alt="">
        </a>
      </div>
      <!-- Post Content -->
      <div class="post-content">
        <a href="#" class="headline">
          <h5 class="mb-0">タグ1 アーカイブ</h5>
        </a>
      </div>
    </div>

    <!-- Single Blog Post -->
    <div class="single-blog-post post-style-2 d-flex align-items-center widget-post">
      <!-- Post Thumbnail -->
      <div class="post-thumbnail">
        <a href="">
          <img src="/static/img/blog-img/b11.jpg" alt="">
        </a>
      </div>
      <!-- Post Content -->
      <div class="post-content">
        <a href="#" class="headline">
          <h5 class="mb-0">タグ2 アーカイブ</h5>
        </a>
      </div>
    </div>

    <!-- Single Blog Post -->
    <div class="single-blog-post post-style-2 d-flex align-items-center widget-post">
      <!-- Post Thumbnail -->
      <div class="post-thumbnail">
        <a href="">
          <img src="/static/img/blog-img/b12.jpg" alt="">
        </a>
      </div>
      <!-- Post Content -->
      <div class="post-content">
        <a href="#" class="headline">
          <h5 class="mb-0">タグ3 アーカイブ</h5>
        </a>
      </div>
    </div>

    <!-- Single Blog Post -->
    <div class="single-blog-post post-style-2 d-flex align-items-center widget-post">
      <!-- Post Thumbnail -->
      <div class="post-thumbnail">
        <a href="">
          <img src="/static/img/blog-img/b13.jpg" alt="">
        </a>
      </div>
      <!-- Post Content -->
      <div class="post-content">
        <a href="#" class="headline">
          <h5 class="mb-0">タグ4 アーカイブ</h5>
        </a>
      </div>
    </div>

    <!-- Single Blog Post -->
    <div class="single-blog-post post-style-2 d-flex align-items-center widget-post">
      <!-- Post Thumbnail -->
      <div class="post-thumbnail">
        <a href="">
          <img src="/static/img/blog-img/b14.jpg" alt="">
        </a>
      </div>
      <!-- Post Content -->
      <div class="post-content">
        <a href="#" class="headline">
          <h5 class="mb-0">タグ5 アーカイブ</h5>
        </a>
      </div>
    </div>

  </div>
</div>
