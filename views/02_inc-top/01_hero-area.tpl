
<div class="hero-area height-500">

  <!-- Hero Slides Area -->
  <div class="hero-slides owl-carousel">
    <!-- Single Slide -->
    <div class="single-hero-slide bg-img background-overlay height-500"
      style="background-image: url(/static/img/bg/bg1.jpg);">
    </div>
    <div class="single-hero-slide bg-img background-overlay height-500"
      style="background-image: url(/static/img/bg/bg2.jpg);">
    </div>

    <div class="single-hero-slide bg-img background-overlay height-500"
      style="background-image: url(/static/img/blog-img/bg1.jpg);">
    </div>
    <!-- Single Slide -->
    <div class="single-hero-slide bg-img background-overlay height-500"
      style="background-image: url(/static/img/blog-img/bg2.jpg);">
    </div>
    <!-- Single Slide -->
    <div class="single-hero-slide bg-img background-overlay height-500"
      style="background-image: url(/static/img/blog-img/bg3.jpg);">
    </div>
    <!-- Single Slide -->
    <div class="single-hero-slide bg-img background-overlay height-500"
      style="background-image: url(/static/img/blog-img/bg4.jpg);">
    </div>
  </div>

  <!-- Hero Post Slide -->
  <div class="hero-title-area">
    <div class="container h-100">
      <div class="row h-100 align-items-center justify-content-center">
        <div class="col-12 col-md-8 col-lg-6">
          <div class="single-blog-title text-center">
            <h1><img src="/static/img/logo/mrrhp-logo-white.png" alt="みろりHP"></h1>
            <h3>Django trial Website</h3>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
