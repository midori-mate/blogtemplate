
<div id="top-cal-heatmap-wrapper" class="world-catagory-area mt-50">
  <ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="title">GitLab Activity</li>
  </ul>

  <div class="tab-content">

    <div class="tab-pane fade show active" >

      <div class="single-blog-post post-style-4 d-flex align-items-center">
        <div id="top-cal-heatmap"></div>
        <script type="text/javascript">
          window.onload = drowCalHeatMap;

          function drowCalHeatMap() {
            var cal = new CalHeatMap();
            var obj = $.parseJSON('{{!heatmap_json}}');
            var now = new Date();
            var start = now;
            start.setMonth(now.getMonth() - 5);
            start.setDate(1);

            cal.init({
              data: obj,
              dataType:     'json',
              start:        start,
              itemSelector: '#top-cal-heatmap',
              domain:       'month',
              domainLabelFormat:  '%Y-%m',
              subDomain:    'day',
              range:        6,
              cellSize:     15,
              cellPadding:  2,
              cellRadius:   0,
              domainGutter: 1,
              domainMargin: [0, 0, 0, 0],
              domainDynamicDimension: false,
              verticalOrientation: false,
              label: {
                position: 'bottom',
                width: 100,
              },
              colLimit:     null,
              rowLimit:     null,
              tooltip:      true,
              highlight:    ['now', now],
              weekStartOnMonday: false,
              minDate:      null,
              maxDate:      null,
              considerMissingDataAsZero: false,
              legend:       [10, 20, 30, 40],
              displayLegend: true,
              legendCellSize: 15,
              legendCellPadding: 2,
              legendMargin: [10, 0, 0, 0],
              legendVerticalPosition: 'bottom',
              legendHorizontalPosition: 'left',
              legendOrientation: 'horizontal',
              legendColors: ['#d2de76', '#51682a'],
              itemName: ['act', 'acts'],
            });
          }
        </script>

      </div>
    </div>

  </div>
</div>
