
Blog Template
===

Create unique html template for blog.

<!-- ![1](media/***.jpg) -->

## Usage

- Open Python bottle server.
    - `$ python BlogTemplate.py`
- View templates on browser.
    - [http://localhost:8000/index.html](http://localhost:8000)

## Description


## Dependency


## Image Tips

|        Type        | Width | Height |
|--------------------|-------|--------|
| Top Background     |  1920 |    500 |
| Article Background |  1920 |    400 |
| Thumbnail          |   360 |    195 |
